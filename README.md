# Simple blog

[![Author](https://img.shields.io/badge/author-Kuroyasha512-blue.svg)](https://gitlab.com/Kuroyasha512)
[![Software License](https://img.shields.io/badge/license-GNU_GPLv3-brightgreen.svg)](https://gitlab.com/Kuroyasha512/Simple-blog#license)
[![Status](https://img.shields.io/badge/Status-Development-red.svg)](https://gitlab.com/Kuroyasha512/Simple-blog/blob/master/README.md)
[![os](https://img.shields.io/badge/os-GNU/Linux-red.svg)](https://www.gnu.org/gnu/linux-and-gnu.en.html)

simple blog Application Use Rails  
Prerequisites :
- Ruby  v2.5.1
- Rails v5.2.2
- Gem   v2.7.6

## Setup
```sh
$ git clone https://github.com/Shinmariu/Ruby-on-Rails-Simple-blog.git
$ cd Ruby-on-Rails-Simple-blog
$ rails s
```
## Password can be change in :
- app/controllers/comments_controller.rb
- app/controllers/articles_controller.rb

## **License**

This project is licensed under the GNU GPLv3 License - see the [LICENSE](https://gitlab.com/Kuroyasha512/keycode/blob/master/LICENSE) file for details

### **Philosophy**
My program use Unix Philosophy :  
The UNIX philosophy is documented by Doug McIlroy in the Bell System Technical Journal from 1978  
1. Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new "features".
2. Expect the output of every program to become the input to another, as yet unknown, program. Don't clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don't insist on interactive input.
3. Design and build software, even operating systems, to be tried early, ideally within weeks. Don't hesitate to throw away the clumsy parts and rebuild them.
4. Use tools in preference to unskilled help to lighten a programming task, even if you have to detour to build the tools and expect to throw some of them out after you've finished using them.
